﻿using System.Linq;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitialiazer : IDbInitializer
    {
        private readonly IMongoCollection<Customer> _customers;
        private readonly IMongoCollection<Preference> _preferences;

        public MongoDbInitialiazer(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _customers = database.GetCollection<Customer>($"{nameof(Customer)}s");
            _preferences = database.GetCollection<Preference>($"{nameof(Preference)}s");
        }

        public void InitializeDb()
        {
            _customers.DeleteMany(r => true);
            _preferences.DeleteMany(r => true);

            _customers.InsertMany(FakeDataFactory.Customers);
            _preferences.InsertMany(FakeDataFactory.Preferences);
        }
    }
}
