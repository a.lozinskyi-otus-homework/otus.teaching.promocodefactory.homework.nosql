﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo
{
    public interface IMongoDatabaseSettings
    {
        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }
}
