﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Mongo
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
