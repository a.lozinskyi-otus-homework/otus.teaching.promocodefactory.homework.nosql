﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
