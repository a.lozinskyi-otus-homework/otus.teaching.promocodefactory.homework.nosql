﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo
{
    public interface IMongoDatabaseSettings
    {
        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }
}
